#!/bin/sh
virt-install --arch=x86_64 --virt-type=kvm --name deb10bit64mn0 --ram 2800 --vcpus=2 --os-type=linux --os-variant=debian10 --virt-type=kvm --hvm --network=bridge=virbr0,model=virtio --graphics vnc --video=qxl --noautoconsole --disk path=/var/lib/libvirt/images/deb10bit64mn0.qcow2,size=8,bus=virtio,format=qcow2 --cdrom=/var/lib/libvirt/iso/debian-10.5.0-amd64-DVD-1.iso


#!/bin/sh
virt-install --arch=x86_64 --virt-type=kvm --name cent7bit64lvm1 --ram 2800 --vcpus=2 --os-type=linux --os-variant=centos7.0 --virt-type=kvm --hvm --network=bridge=virbr0,model=virtio --graphics vnc --video=qxl --noautoconsole --disk path=/var/lib/libvirt/images/cent7bit64lvm1.qcow2,size=8,bus=virtio,format=qcow2 --cdrom=/var/lib/libvirt/iso/CentOS-7-x86_64-DVD-2003.iso

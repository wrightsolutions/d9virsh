#!/bin/dash
## "Debian GNU/Linux 11 (bullseye)" ; "Debian GNU/Linux" ; bullseye
# 20211226T162231 - Total of 175 packages will be listed below
apt-get install adduser apparmor apt apt-listchanges apt-utils aptitude aptitude-common;
apt-get install augeas-lenses augeas-tools base-files base-passwd bash bash-completion;
apt-get install bind9-host binutils bridge-utils bsdmainutils bsdutils busybox bzip2;
apt-get install ca-certificates chrony cloud-guest-utils cloud-image-utils cloud-init;
apt-get install cloud-initramfs-growroot cloud-utils console-setup coreutils cpio cron;
apt-get install curl dash dbus debconf debconf-i18n debian-archive-keyring debianutils;
apt-get install debootstrap diffutils distro-info-data dmidecode dmsetup dnsmasq;
apt-get install dosfstools downtimed dpkg duperemove e2fsprogs emacs-bin-common ethtool;
apt-get install findutils gawk gnupg gpgv grep grub-cloud-amd64 gzip hostname htop;
apt-get install ifupdown init init-system-helpers iptables iputils-ping;
apt-get install isc-dhcp-client jq less libaudit-common libc-bin;
apt-get install liblocale-gettext-perl libmagic-mgc libpam-modules-bin;
apt-get install libpam-runtime libsemanage-common libtext-charwidth-perl;
apt-get install libtext-iconv-perl libtext-wrapi18n-perl libtirpc-common;
apt-get install libvirt-clients libvirt-daemon libvirt-daemon-system libxml2-utils;
apt-get install linux-image-amd64 locales locales-all login logrotate logsave lsb-base;
apt-get install lsb-release lsof lvm2 man-db manpages mawk mdadm mime-support most mount;
apt-get install nano ncurses-base ncurses-bin netbase openssh-server;
apt-get install openssh-sftp-server openssl parted passwd pciutils perl-base procps;
apt-get install psmisc python-is-python2 python2-minimal python2.7 python2.7-minimal;
apt-get install python3 python3-asn1crypto python3-blinker python3-certifi;
apt-get install python3-chardet python3-configobj python3-cryptography;
apt-get install python3-guestfs python3-idna python3-jinja2 python3-json-pointer;
apt-get install python3-jsonpatch python3-jsonschema python3-jwt python3-markupsafe;
apt-get install python3-minimal python3-oauthlib python3-pkg-resources;
apt-get install python3-requests python3-six python3-urllib3 python3-venv;
apt-get install python3-yaml python3.9 python3.9-minimal qemu-system-common;
apt-get install qemu-system-x86 qemu-utils readline-common reportbug resolvconf rinetd;
apt-get install rsyslog screen sed sensible-utils socat squashfs-tools ssl-cert sudo;
apt-get install sysstat systemd systemd-sysv sysvinit-utils tar tcpdump traceroute;
apt-get install tzdata unattended-upgrades util-linux uuid-runtime vim vim-tiny;
apt-get install whiptail xauth xxd xz-utils zile;

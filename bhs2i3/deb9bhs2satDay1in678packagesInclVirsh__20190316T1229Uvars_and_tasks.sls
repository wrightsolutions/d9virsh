# Salt suitable list of packages to install generated by
# ./base2manual.py 'sls' 'manual' 'all'
## "Debian GNU/Linux 9 (stretch)" ; "Debian GNU/Linux"
# 20190316T124525 - Total of 678 packages detected
# 20190316T124525 - Total of 191 'manual' packages will be listed below
base2manualA_install:
  pkg.installed:
    - pkgs:
      - adduser
      - amd64-microcode
      - apt
      - apt-config-auto-update
      - apt-transport-https
      - apt-utils
      - aptitude
      - augeas-tools

base2manualB_install:
  pkg.installed:
    - pkgs:
      - base-files
      - base-passwd
      - bash
      - bc
      - bind9
      - bsdmainutils
      - bsdutils
      - btrfs-progs
      - btrfs-tools
      - busybox
      - bzip2

base2manualC_install:
  pkg.installed:
    - pkgs:
      - ca-certificates
      - console-setup
      - coreutils
      - cpio
      - cpufrequtils
      - cron
      - curl

base2manualD_install:
  pkg.installed:
    - pkgs:
      - dash
      - dbus
      - debconf
      - debconf-i18n
      - debconf-utils
      - debian-archive-keyring
      - debianutils
      - diffutils
      - discover
      - dmidecode
      - dmsetup
      - dnsmasq
      - dnsutils
      - dosfstools
      - downtimed
      - dpkg

base2manualE_install:
  pkg.installed:
    - pkgs:
      - e2fsprogs
      - eject
      - ethtool

base2manualF_install:
  pkg.installed:
    - pkgs:
      - findutils
      - firewalld

base2manualG_install:
  pkg.installed:
    - pkgs:
      - gdisk
      - genisoimage
      - git
      - glances
      - gnupg
      - gnupg-agent
      - gpgv
      - grep
      - grub-common
      - grub-pc
      - gzip

base2manualH_install:
  pkg.installed:
    - pkgs:
      - haveged
      - hddtemp
      - host
      - hostname

base2manualI_install:
  pkg.installed:
    - pkgs:
      - icinga-cgi
      - icinga-common
      - icinga-core
      - ifupdown
      - init
      - init-system-helpers
      - initramfs-tools
      - installation-report
      - intel-microcode
      - ipmitool
      - iproute2
      - iptables
      - iputils-clockdiff
      - iputils-ping
      - iputils-tracepath
      - irqbalance
      - isc-dhcp-client
      - isc-dhcp-common
      - iucode-tool

base2manualK_install:
  pkg.installed:
    - pkgs:
      - keyboard-configuration
      - kmod

base2manualL_install:
  pkg.installed:
    - pkgs:
      - laptop-detect
      - less
      - libaudit-common
      - libc-bin
      - libdns-export162
      - libestr0
      - libguestfs-tools
      - libio-socket-ssl-perl
      - libisc-export160
      - liblocale-gettext-perl
      - libmonitoring-plugin-perl
      - libosinfo-bin
      - libpam-modules-bin
      - libpam-runtime
      - libsemanage-common
      - libtext-charwidth-perl
      - libtext-iconv-perl
      - libtext-wrapi18n-perl
      - libvirt-clients
      - libvirt-daemon
      - libvirt-daemon-system
      - libxml-simple-perl
      - libxml2-utils
      - linux-image-amd64
      - locales
      - login
      - logrotate
      - lsb-base
      - lsb-release
      - lvm2

base2manualM_install:
  pkg.installed:
    - pkgs:
      - man-db
      - manpages-de
      - manpages-es
      - manpages-fr
      - manpages-it
      - manpages-pl
      - manpages-pt
      - mawk
      - mdadm
      - monitoring-plugins-basic
      - monitoring-plugins-common
      - monitoring-plugins-standard
      - mount
      - mtr-tiny
      - multiarch-support

base2manualN_install:
  pkg.installed:
    - pkgs:
      - nagios-images
      - nagios-nrpe-plugin
      - nagios-nrpe-server
      - nagios-plugins-common
      - nagios-plugins-standard
      - nano
      - ncurses-base
      - ncurses-bin
      - net-tools
      - netbase
      - nethogs
      - ntpdate
      - nvme-cli

base2manualO_install:
  pkg.installed:
    - pkgs:
      - openssh-server
      - openssl

base2manualP_install:
  pkg.installed:
    - pkgs:
      - parted
      - passwd
      - pciutils
      - perl
      - perl-base
      - pinentry-curses
      - procps
      - python-ethtool
      - python-libvirt
      - python-zmq

base2manualQ_install:
  pkg.installed:
    - pkgs:
      - qemu-kvm
      - qemu-system-common

base2manualR_install:
  pkg.installed:
    - pkgs:
      - readline-common
      - reiserfsprogs
      - rinetd
      - rsync
      - rsyslog

base2manualS_install:
  pkg.installed:
    - pkgs:
      - salt-common
      - salt-ssh
      - screen
      - sed
      - sensible-utils
      - smartmontools
      - sudo
      - sysstat
      - systemd
      - systemd-sysv
      - sysvinit-utils

base2manualT_install:
  pkg.installed:
    - pkgs:
      - tar
      - task-english
      - tasksel
      - tasksel-data
      - tcpdump
      - tzdata

base2manualU_install:
  pkg.installed:
    - pkgs:
      - udev
      - usbutils
      - util-linux

base2manualV_install:
  pkg.installed:
    - pkgs:
      - vim
      - vim-common
      - vim-tiny
      - virt-manager
      - vlan

base2manualW_install:
  pkg.installed:
    - pkgs:
      - wget
      - whiptail

base2manualX_install:
  pkg.installed:
    - pkgs:
      - xauth
      - xfsprogs
      - xxd

base2manualZ_install:
  pkg.installed:
    - pkgs:
      - zile


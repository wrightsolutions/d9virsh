#!/bin/dash
## "Debian GNU/Linux 9 (stretch)" ; "Debian GNU/Linux"
# 20190316T124522 - Total of 191 packages will be listed below
apt-get install adduser amd64-microcode apt apt-config-auto-update;
apt-get install apt-transport-https apt-utils aptitude augeas-tools base-files;
apt-get install base-passwd bash bc bind9 bsdmainutils bsdutils btrfs-progs btrfs-tools;
apt-get install busybox bzip2 ca-certificates console-setup coreutils cpio cpufrequtils;
apt-get install cron curl dash dbus debconf debconf-i18n debconf-utils;
apt-get install debian-archive-keyring debianutils diffutils discover dmidecode;
apt-get install dmsetup dnsmasq dnsutils dosfstools downtimed dpkg e2fsprogs eject;
apt-get install ethtool findutils firewalld gdisk genisoimage git glances gnupg;
apt-get install gnupg-agent gpgv grep grub-common grub-pc gzip haveged hddtemp host;
apt-get install hostname icinga-cgi icinga-common icinga-core ifupdown init;
apt-get install init-system-helpers initramfs-tools installation-report;
apt-get install intel-microcode ipmitool iproute2 iptables iputils-clockdiff;
apt-get install iputils-ping iputils-tracepath irqbalance isc-dhcp-client;
apt-get install isc-dhcp-common iucode-tool keyboard-configuration kmod laptop-detect;
apt-get install less libaudit-common libc-bin libdns-export162 libestr0;
apt-get install libguestfs-tools libio-socket-ssl-perl libisc-export160;
apt-get install liblocale-gettext-perl libmonitoring-plugin-perl libosinfo-bin;
apt-get install libpam-modules-bin libpam-runtime libsemanage-common;
apt-get install libtext-charwidth-perl libtext-iconv-perl libtext-wrapi18n-perl;
apt-get install libvirt-clients libvirt-daemon libvirt-daemon-system;
apt-get install libxml-simple-perl libxml2-utils linux-image-amd64 locales login;
apt-get install logrotate lsb-base lsb-release lvm2 man-db manpages-de manpages-es;
apt-get install manpages-fr manpages-it manpages-pl manpages-pt mawk mdadm;
apt-get install monitoring-plugins-basic monitoring-plugins-common;
apt-get install monitoring-plugins-standard mount mtr-tiny multiarch-support;
apt-get install nagios-images nagios-nrpe-plugin nagios-nrpe-server;
apt-get install nagios-plugins-common nagios-plugins-standard nano ncurses-base;
apt-get install ncurses-bin net-tools netbase nethogs ntpdate nvme-cli openssh-server;
apt-get install openssl parted passwd pciutils perl perl-base pinentry-curses procps;
apt-get install python-ethtool python-libvirt python-zmq qemu-kvm qemu-system-common;
apt-get install readline-common reiserfsprogs rinetd rsync rsyslog salt-common salt-ssh;
apt-get install screen sed sensible-utils smartmontools sudo sysstat systemd;
apt-get install systemd-sysv sysvinit-utils tar task-english tasksel tasksel-data;
apt-get install tcpdump tzdata udev usbutils util-linux vim vim-common vim-tiny;
apt-get install virt-manager vlan wget whiptail xauth xfsprogs xxd zile;

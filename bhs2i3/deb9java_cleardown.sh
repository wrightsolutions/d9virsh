#!/bin/sh
dpkg --purge openjdk-8-jre-headless default-jre-headless openjdk-8-jre default-jre \
ant ant-optional ca-certificates-java \
libmsv-java libdom4j libdom4j-java libsaxonhe-java libapache-poi-java libxmlbeans-java \
libreoffice-nlpsolver liblayout-java libpentaho-reporting-flow-engine-java \
libreoffice-report-builder libreoffice-script-provider-js \
libreoffice-sdbc-hsqldb libreoffice-script-provider-bsh

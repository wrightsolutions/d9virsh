#!/bin/dash
## "Debian GNU/Linux 8 (jessie)" ; "Debian GNU/Linux"
# 20190705T102255 - Total of 177 packages will be listed below
apt-get install accountsservice acl acpid adduser anacron apt apt-listchanges;
apt-get install apt-transport-https apt-utils apt-xapian-index aptitude;
apt-get install aptitude-common arping at avahi-daemon base-files base-passwd bash;
apt-get install bash-completion bc binutils bsd-mailx bsdmainutils bsdutils busybox;
apt-get install bzip2 cgmanager console-data console-setup consolekit coreutils cpio;
apt-get install cron curl dash dbus dc debconf debconf-i18n debian-archive-keyring;
apt-get install debian-faq debianutils diffutils dmidecode dnsutils doc-debian;
apt-get install dosfstools dpkg e2fsprogs ed eject exim4 findutils firefox-esr ftp fuse;
apt-get install gettext git git-man glances gnupg gpgv grep groff-base grub-legacy gzip;
apt-get install hdparm heirloom-mailx hostname ifupdown info initramfs-tools;
apt-get install initscripts insserv install-info installation-report iproute iproute2;
apt-get install iptables iputils-ping iputils-tracepath isc-dhcp-client;
apt-get install isc-dhcp-common keyboard-configuration klibc-utils kmod;
apt-get install libboost-iostreams1.49.0 libc-bin liberror-perl libklibc;
apt-get install liblocale-gettext-perl libnih-dbus1 libnih1 libpam-modules-bin;
apt-get install libpam-runtime libsemanage-common libtext-charwidth-perl;
apt-get install libtext-iconv-perl libtext-wrapi18n-perl libxapian22;
apt-get install linux-image-amd64 locales login logrotate lsb-base lshw lsof lzip m4;
apt-get install makedev man-db manpages mawk mktemp mlocate mount multiarch-support;
apt-get install ncurses-base ncurses-bin ncurses-term net-tools netbase netcat;
apt-get install netcat-traditional ntpdate parted passwd patch perl-base procps psmisc;
apt-get install python-levenshtein readline-common rsync rsyslog salt-common salt-ssh;
apt-get install sed sensible-utils shared-mime-info slim squashfs-tools ssh ssl-cert;
apt-get install sudo systemd systemd-sysv sysv-rc sysvinit sysvinit-utils tar;
apt-get install task-english tasksel tasksel-data tcpdump texinfo time traceroute tzdata;
apt-get install ucf udev unzip update-inetd util-linux util-linux-locales uuid-runtime;
apt-get install vim-common vim-tiny w3m wamerican wget whiptail whois xfce4;
apt-get install xserver-xorg-input-kbd xz-utils zile;

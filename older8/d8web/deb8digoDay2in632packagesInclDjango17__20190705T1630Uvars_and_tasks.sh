#!/bin/dash
## "Debian GNU/Linux 8 (jessie)" ; "Debian GNU/Linux"
# 20190705T152624 - Total of 199 packages will be listed below
apt-get install accountsservice acct acl acpid adduser anacron apache2 apache2-bin;
apt-get install apache2-doc apt apt-listchanges apt-transport-https apt-utils;
apt-get install apt-xapian-index aptitude aptitude-common arping at avahi-daemon;
apt-get install base-files base-passwd bash bash-completion bc binutils bsd-mailx;
apt-get install bsdmainutils bsdutils busybox bzip2 cgmanager console-data;
apt-get install console-setup consolekit coreutils cpio cron curl dash dbus dc debconf;
apt-get install debconf-i18n debconf-utils debian-archive-keyring debian-faq;
apt-get install debianutils diffutils dmidecode dnsutils doc-debian dosfstools dpkg;
apt-get install e2fsprogs ed eject exim4 findutils firewalld ftp fuse gawk gettext;
apt-get install git git-man glances gnupg gpgv grep groff-base grub-legacy gzip hdparm;
apt-get install heirloom-mailx hostname htop ifupdown info initramfs-tools initscripts;
apt-get install insserv install-info installation-report iperf iproute iproute2;
apt-get install iptables iputils-ping iputils-tracepath isc-dhcp-client;
apt-get install isc-dhcp-common keyboard-configuration klibc-utils kmod ldnsutils lftp;
apt-get install libapache2-mod-wsgi libboost-iostreams1.49.0 libc-bin liberror-perl;
apt-get install libjs-jquery libklibc liblocale-gettext-perl libnih-dbus1 libnih1;
apt-get install libpam-modules-bin libpam-runtime libsemanage-common;
apt-get install libtext-charwidth-perl libtext-iconv-perl libtext-wrapi18n-perl;
apt-get install libxapian22 linux-image-amd64 locales login logrotate lsb-base lshw lsof;
apt-get install lzip m4 makedev man-db manpages mawk mercurial mktemp mlocate mount;
apt-get install mtr-tiny multiarch-support ncurses-base ncurses-bin ncurses-term;
apt-get install net-tools netbase netcat netcat-traditional ntpdate parted passwd patch;
apt-get install perl-base procps psmisc python-django python-levenshtein;
apt-get install python-memcache python-sqlparse python-tz python-werkzeug;
apt-get install python-werkzeug-doc readline-common rsync rsyslog salt-common salt-ssh;
apt-get install sed sensible-utils shared-mime-info slim squashfs-tools ssh ssl-cert;
apt-get install sudo sysstat systemd systemd-sysv sysv-rc sysvinit sysvinit-utils tar;
apt-get install task-english tasksel tasksel-data tcpdump tcptraceroute telnet-ssl;
apt-get install texinfo time traceroute tzdata ucf udev unhide unzip update-inetd;
apt-get install util-linux util-linux-locales uuid-runtime vim-common vim-tiny w3m;
apt-get install wamerican wget whiptail whois xz-utils zile;

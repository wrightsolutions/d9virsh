#!/bin/dash
## "Debian GNU/Linux 8 (jessie)" ; "Debian GNU/Linux"
# 20190705T103913 - Total of 199 packages will be listed below
apt-get install accountsservice acct acl acpid adduser anacron apache2.2-common apt;
apt-get install apt-listchanges apt-transport-https apt-utils apt-xapian-index;
apt-get install aptitude aptitude-common at avahi-daemon base-files base-passwd bash;
apt-get install bash-completion bc binutils bsd-mailx bsdmainutils bsdutils busybox;
apt-get install bzip2 ca-certificates checksecurity clamav console-data console-setup;
apt-get install consolekit coreutils cpio cron cron-apt curl dash dbus dc debconf;
apt-get install debconf-i18n debconf-utils debian-archive-keyring debian-faq;
apt-get install debianutils diffutils dmidecode dnsutils doc-debian dosfstools;
apt-get install downtimed dpkg dropbear e2fsprogs ed eject exim4 findutils fping ftp;
apt-get install fuse gawk gettext git glances gnupg gpgv grep groff-base grub-legacy;
apt-get install gzip hdparm heirloom-mailx hostname htop ifupdown info initramfs-tools;
apt-get install initscripts insserv install-info installation-report iperf iproute;
apt-get install iptables iputils-ping iputils-tracepath isc-dhcp-client;
apt-get install isc-dhcp-common keyboard-configuration klibc-utils kmod ldnsutils lftp;
apt-get install libapache2-mod-wsgi libboost-iostreams1.49.0 libc-bin;
apt-get install libhtml-parser-perl libklibc liblocale-gettext-perl;
apt-get install libpam-modules-bin libpam-runtime libsemanage-common;
apt-get install libtext-charwidth-perl libtext-iconv-perl libtext-wrapi18n-perl;
apt-get install libxapian22 linux-image-3.2.0-4-amd64 linux-image-amd64 locales login;
apt-get install logrotate lsb-base lshw lzip m4 makedev man-db manpages mawk mercurial;
apt-get install mktemp mlocate mount mtr-tiny multiarch-support mutt nano ncurses-base;
apt-get install ncurses-bin ncurses-term net-tools netbase netcat netcat-traditional;
apt-get install nethogs ntpdate openssl parted passwd perl-base procps psmisc;
apt-get install python-croniter python-tornado python3-minimal readline-common rinetd;
apt-get install rkhunter rsync rsyslog ruby1.9.3 runit salt-common salt-minion sed;
apt-get install sensible-utils shared-mime-info shorewall shorewall6 ssh ssl-cert;
apt-get install strace sudo sysstat systemd-sysv sysv-rc sysvinit sysvinit-utils tar;
apt-get install task-english tasksel tasksel-data tcpdump tcptraceroute telnet-ssl;
apt-get install texinfo time traceroute tzdata ucf udev unhide unzip update-inetd;
apt-get install util-linux util-linux-locales uuid-runtime vim-common vim-tiny w3m;
apt-get install wamerican wget whiptail whois xz-utils zile;

#!/bin/sh
apt-get install acct arping debconf-utils cron curl dash gawk htop iperf
apt-get install iperf iproute2 iputils-ping iputils-tracepath
apt-get install ldnsutils lftp lshw lzip mercurial mtr-tiny
apt-get install netcat ntpdate parted psmisc
apt-get install rsync squashfs-tools sudo sysstat tcptraceroute telnet-ssl
apt-get install traceroute unhide unzip w3m wget whiptail zile
dpkg --purge firefox-esr xfce4 xserver-xorg-input-kbd
dpkg --purge libgl1-mesa-dri desktop-base xserver-xorg-core xfonts-base \
  adwaita-icon-theme tango-icon-theme libgtk-3-common libgtk2.0-common

#dpkg --purge cgmanager libnih1 libnih-dbus1
